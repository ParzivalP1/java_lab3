package com.company;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        GenericItem item1 = new GenericItem(0, "apple", 50, Category.FOOD);
        GenericItem item2 = new GenericItem(1, "pear", 35, Category.FOOD);
        GenericItem item3 = new GenericItem(2, "banana", 40, Category.FOOD);
        GenericItem item4 = new GenericItem(3, "jeans", 1000, Category.DRESS);

        item1.printALL();
        item2.printALL();
        item3.printALL();

        item4.compareItems(item1);

        LocalDate date1 = LocalDate.now().minusDays(3);

        FoodItem item5 = new FoodItem(4, "milk", 89F, Category.FOOD, date1, (short) 5);
        TechnicalItem item6 = new TechnicalItem(5, "phone", 75000, Category.GENERAL, (short) 365);

        ArrayList<GenericItem> arrayList1 = new ArrayList<>();
        arrayList1.add(item5);
        arrayList1.add(item6);

        for (var item: arrayList1) {
            item.printALL();
        }

        U0901Main test1 = new U0901Main();
        test1.main();

        String line = "Конфеты ’Маска’;45;120";
        String[] item_fld = line.split(";");


        FoodItem item7 = new FoodItem(item_fld[0], Float.parseFloat(item_fld[1]), Short.parseShort(item_fld[2]));

        item7.printALL();

    }
}
