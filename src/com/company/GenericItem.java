package com.company;

import java.time.LocalDate;

enum Category {
    FOOD,
    PRINT,
    DRESS,
    GENERAL
}

class GenericItem {
    public int ID;
    public String name;
    public float price;
    public Category type;

    static int currentID;

    public GenericItem(String name, float price, Category category){
        this.ID = GenericItem.currentID++;
    }
    public GenericItem(String name, float price, GenericItem analog){
        this.ID = GenericItem.currentID++;
    }
    public GenericItem(){
        this.ID = GenericItem.currentID++;
    }


    public GenericItem(int newId, String newName,float newPrice, Category newType){
        ID = newId;
        name = newName;
        price = newPrice;
        type = newType;
        this.ID = GenericItem.currentID++;
    }

    void printALL(){
        System.out.printf("ID: %d , Name: %s , price: %5.2f , Type: %s \n", ID, name, price, type);
    }

    void compareItems(GenericItem item){
        if (this.type == item.type){
            System.out.printf("Item: %s is similar to Item: %s \n", this.name, item.name);
        }
        else
        {
            System.out.printf("Item: %s is not similar to Item: %s \n", this.name, item.name);
        }
    }
}
class FoodItem extends GenericItem{
    LocalDate dateOfIncome;
    short expires;

    FoodItem similar;

    public FoodItem(String newName, float newPrice, FoodItem analog, LocalDate date, short newExpires){
        name = newName;
        price = newPrice;
        dateOfIncome = date;
        expires = newExpires;
        similar = analog;
    }
    public FoodItem(String name, float newPrice, short newExpires){
        this(name,newPrice,null, LocalDate.now(),newExpires);

    }
    public FoodItem(String name){
        this(name, 0F, null, null, (short) 0);
    }


    public FoodItem(int newId, String newName,float newPrice, Category newType, LocalDate newDate, short expirePeriod){
        super(newId, newName, newPrice, newType);
        dateOfIncome = newDate;
        expires = expirePeriod;
    }

    @Override
    void printALL(){
        System.out.printf("ID: %d , Name: %s , price: %5.2f , Type: %s, Date of income: %tA , Expires: %d \n", ID, name, price, type, dateOfIncome, expires);
    }
}
class TechnicalItem extends GenericItem{
    short warrantyTime;

    public TechnicalItem(int newId, String newName, float newPrice, Category newType, short newWarrantyTime){
        super(newId, newName, newPrice, newType);
        warrantyTime = newWarrantyTime;
    }

    @Override
    void printALL(){
        System.out.printf("ID: %d , Name: %s , price: %5.2f , Type: %s, Warranty time: %d \n", ID, name, price, type, warrantyTime);
    }
}

class U0901WorkArray <T extends Number> {

    T[] arrNums;

    U0901WorkArray(T[] numP){
        arrNums = numP;
    }

    double sum(){
        double doubleWork = 0D;

        for (var i:arrNums){
            doubleWork += i.doubleValue();
        }

        return doubleWork;
    }
}

class U0901Main{

    void main(){
        Integer[] intArr ={10,20,15};
        Float[] floatArr = new Float[3];
        // String[] strArr = {"q","a"};

        float randMax = 10f;

        for(int i = 0; i < floatArr.length; i++){
            floatArr[i] = (float) Math.random() * ++randMax;
        }

        U0901WorkArray insWorkArrayInt = new U0901WorkArray(intArr);
        U0901WorkArray insWorkArrayFloat = new U0901WorkArray(floatArr);
        // U0901WorkArray insWorkArrayStr = new U0901WorkArray(strArr);

        System.out.printf("\nInt sum: %f \nFloat sum: %f \n", insWorkArrayInt.sum(), insWorkArrayFloat.sum());
    }
}
